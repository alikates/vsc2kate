# Visual Studio Code to Kate snippet converter

To use it:
```
python vsc2kate.py -n <snippet group name> -a <author> -i <input.json> -o <output.xml>
```
