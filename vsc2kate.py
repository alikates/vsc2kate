#!/usr/bin/python

import xml.etree.ElementTree as ET
import json
import sys
import re

import argparse

def vsc_placeholder_to_kate(m):
	placeholder_parts = re.match("^(\$\{)(([^|]*)(\|[^|]+\|))+(\})$|^(\$)\{?(\w+)\}?$|^(\${)(\w+:)([^:]+)(\})$", m.group())
	if placeholder_parts != None:
		if placeholder_parts.groups()[0] == None and placeholder_parts.groups()[5] != None:	# style $1
			if placeholder_parts.groups()[6] == '0':
				return "${cursor}"
			return "${}".format(placeholder_parts.groups()[6])
		elif (placeholder_parts.groups()[0] != None and placeholder_parts.groups()[3] != None and placeholder_parts.groups()[4] != None):
			field_name = placeholder_parts.group(3)
			field_defaults = placeholder_parts.group(4).replace("|", "[\"", 1).replace(",", "\",\"").replace("|", "\"]", 1)
			return field_name + "=" + field_defaults
		elif placeholder_parts.groups()[7] != None and placeholder_parts.groups()[9] != None:
			return "${{{}}}".format(placeholder_parts.groups()[9])

	return ""

def vsc2k(vsc_snippets_file, kate_snippets_file, name, author):

	snippet_file = open(vsc_snippets_file)
	snippets = json.load(snippet_file)

	kate_snippets = ET.Element('snippets', {'author': author, 'name': name})

	script = ET.SubElement(kate_snippets, 'script')
	script.text =  "function fileName() { return document.fileName(); }  \
					function fileUrl() { return document.url(); }        \
					function encoding() { return document.encoding(); }  \
					function selection() { return view.selectedText(); } \
					function year() { return new Date().getFullYear(); } \
					function upper(x) { return x.toUpperCase(); }        \
					function lower(x) { return x.toLowerCase(); }"

	for key in snippets:
		prefix = snippets[key]['prefix']
		body = '\n'.join(snippets[key]['body'])
		if type(prefix) == list:
			prefix = prefix[0]

		# body = re.sub("\$0", "${cursor}", body)
		body = re.sub("\$\{[^{}]*\}|\$\w", vsc_placeholder_to_kate, body)

		snippet_root = ET.SubElement(kate_snippets, 'item')

		snippet_match = ET.SubElement(snippet_root, 'match')
		snippet_match.text = prefix

		snippet_fillin = ET.SubElement(snippet_root, 'fillin')
		snippet_fillin.text = body

	ET.indent(kate_snippets)
	et = ET.ElementTree(kate_snippets)
	et.write(kate_snippets_file)


parser = argparse.ArgumentParser(description='Visual Studio Code to Kate snippets converter')
parser.add_argument('-n', dest="name", required=True)
parser.add_argument('-a', dest="author", required=False, default="vsc2kate")
parser.add_argument('-i', dest="input", required=True)
parser.add_argument('-o', dest="output", required=True)

args = parser.parse_args()

vsc2k(args.input, args.output, args.name, args.author)
